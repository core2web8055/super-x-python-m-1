# qrcode_generator_widget.py
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel

class QrCodeGeneratorWidget(QWidget):
    def __init__(self):
        super().__init__()

        layout = QVBoxLayout()

        label = QLabel("QR Code Generator Widget")
        label.setStyleSheet("font-size: 18px;")

        layout.addWidget(label)

        self.setLayout(layout)
