# calculator_widget.py
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel

class CalculatorWidget(QWidget):
    def __init__(self):
        super().__init__()

        layout = QVBoxLayout()

        label = QLabel("Calculator Widget")
        label.setStyleSheet("font-size: 18px;")

        layout.addWidget(label)

        self.setLayout(layout)
