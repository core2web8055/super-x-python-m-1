# login_page.py
from PyQt5.QtWidgets import QDialog, QLabel, QLineEdit, QVBoxLayout, QPushButton

class LoginPage(QDialog):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Login - Core2Web")
        self.setGeometry(300, 300, 400, 200)

        layout = QVBoxLayout()

        title_label = QLabel("Core2Web")
        title_label.setStyleSheet("font-size: 20px; font-weight: bold;")

        username_label = QLabel("Username:")
        self.username_input = QLineEdit()

        password_label = QLabel("Password:")
        self.password_input = QLineEdit()
        self.password_input.setEchoMode(QLineEdit.Password)

        login_button = QPushButton("Login")
        login_button.clicked.connect(self.handle_login)

        layout.addWidget(title_label)
        layout.addWidget(username_label)
        layout.addWidget(self.username_input)
        layout.addWidget(password_label)
        layout.addWidget(self.password_input)
        layout.addWidget(login_button)

        self.setLayout(layout)

    def handle_login(self):
        # Add logic for handling login credentials
        # For simplicity, let's assume a successful login for now
        self.accept()
