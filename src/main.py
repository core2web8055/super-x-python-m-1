import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QPushButton, QLineEdit

class CalculatorWidget(QWidget):
    def __init__(self):
        super().__init__()

        self.layout = QVBoxLayout()

        self.result_display = QLineEdit(self)
        self.layout.addWidget(self.result_display)

        buttons = [
            ('7', '8', '9', '/'),
            ('4', '5', '6', '*'),
            ('1', '2', '3', '-'),
            ('0', '.', '=', '+'),
        ]

        for row in buttons:
            button_row = QWidget(self)
            button_layout = QVBoxLayout(button_row)

            for button_text in row:
                button = QPushButton(button_text, self)
                button.clicked.connect(lambda _, text=button_text: self.handle_button_click(text))
                button_layout.addWidget(button)

            self.layout.addWidget(button_row)

        self.setLayout(self.layout)

    def handle_button_click(self, button_text):
        if button_text == '=':
            try:
                result = eval(self.result_display.text())
                self.result_display.setText(str(result))
            except Exception as e:
                self.result_display.setText("Error")
        else:
            current_text = self.result_display.text()
            self.result_display.setText(current_text + button_text)

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.login_widget = QWidget(self)
        self.setCentralWidget(self.login_widget)

        self.login_layout = QVBoxLayout(self.login_widget)

        calculator_button = QPushButton("Open Calculator")
        calculator_button.clicked.connect(self.open_calculator)
        self.login_layout.addWidget(calculator_button)

    def open_calculator(self):
        calculator = CalculatorWidget()
        calculator.show()

def main():
    app = QApplication(sys.argv)
    app.setStyle('Fusion')  # Use the Fusion style for a modern look
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()

def main():
    app = QApplication(sys.argv)
    app.setStyle('Fusion')  # Use the Fusion style for a modern look
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
