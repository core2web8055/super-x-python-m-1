# home_screen.py
from PyQt5.QtWidgets import QMainWindow, QMenuBar, QAction, QStackedWidget

from calculator_widget import CalculatorWidget
from qrcode_generator_widget import QrCodeGeneratorWidget
from merge_pdf_widget import MergePdfWidget

class HomeScreen(QMainWindow):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Home - Core2Web")
        self.setGeometry(100, 100, 800, 600)

        menubar = self.menuBar()
        file_menu = menubar.addMenu("File")

        home_action = QAction("Home", self)
        home_action.triggered.connect(self.show_home)
        file_menu.addAction(home_action)

        about_action = QAction("About", self)
        about_action.triggered.connect(self.show_about)
        file_menu.addAction(about_action)

        self.stacked_widget = QStackedWidget()
        self.setCentralWidget(self.stacked_widget)

        # Add the initial widget (Home)
        self.show_home()

    def show_home(self):
        # Load and display the home screen
        self.stacked_widget.setCurrentIndex(0)

    def show_about(self):
        # Load and display the about screen
        self.stacked_widget.setCurrentIndex(1)
