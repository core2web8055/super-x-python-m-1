# app.py
from flask import Flask, render_template, redirect, url_for

app = Flask(__name__)

# Initial vote count
vote_count = 0

@app.route('/')
def generate_qr():
    # Redirect to the vote page after scanning the QR code
    return redirect(url_for('vote'))

@app.route('/vote')
def vote():
    global vote_count
    return render_template('vote.html', vote_count=vote_count)

@app.route('/submit_vote')
def submit_vote():
    global vote_count
    # Increase the vote count
    vote_count += 1
    # Redirect back to the vote page after submitting the vote
    return redirect(url_for('vote'))

if __name__ == '__main__':
    app.run(debug=True)
