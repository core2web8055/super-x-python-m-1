# app.py
from flask import Flask, render_template, request
import firebase_admin
from firebase_admin import credentials, db

cred = credentials.Certificate("serviceAccountKey.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://cludfunctions-ad912-default-rtdb.firebaseio.com/'
})

app = Flask(__name__)

@app.route('/')
def index():
    # Here you can add logic to fetch candidate information from the database
    # For demonstration purposes, let's assume candidate John Doe
    candidate_name = "John Doe"
    return render_template('index.html', candidate_name=candidate_name)

@app.route('/vote', methods=['POST'])
def vote():
    candidate_id = request.form['candidate_id']
    # Update the vote count for the candidate in the Firebase Realtime Database
    ref = db.reference(f'/candidates/{candidate_id}/voteCount')
    ref.transaction(lambda current_value: (current_value or 0) + 1)
    return 'Vote submitted successfully!'

if __name__ == '__main__':
    app.run(debug=True)
