# app.py
from flask import render_template, request
from flask import Flask
import firebase_admin
from firebase_admin import credentials, db

cred = credentials.Certificate("serviceAccountKey.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://cludfunctions-ad912-default-rtdb.firebaseio.com/'
})

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/submit', methods=['POST'])
def submit():
    data = request.form['data']
    ref = db.reference('/data')
    ref.push().set({'data': data})
    return 'Data submitted successfully!'

if __name__ == '__main__':
    app.run(debug=True)
