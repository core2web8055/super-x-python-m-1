# Import the firebase_admin module and initialize Firebase

# Import the firebase module from firebase_integration
from firebase_integration import firebase

# Now you can use the firebase module in your code

import firebase_admin
from firebase_admin import credentials

# Initialize Firebase with the credentials file downloaded from Firebase console
cred = credentials.Certificate("flask_poject/flask_server/credentials.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://authdemo-b309c-default-rtdb.firebaseio.com/'
})

# Now you can interact with the Firebase Realtime Database
