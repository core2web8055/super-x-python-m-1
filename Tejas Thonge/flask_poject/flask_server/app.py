from flask import Flask, request, jsonify
from firebase_admin import credentials, initialize_app, db

app = Flask(__name__)

# Initialize Firebase with the credentials file and database URL
cred = credentials.Certificate("flask_server/credentials.json")
initialize_app(cred, {
    'databaseURL': 'https://authdemo-b309c-default-rtdb.firebaseio.com/'
})

# Reference to your Firebase database
firebase = db.reference()

# Function to increment the vote count in Firebase
def vote_increment():
    # Retrieve the current vote count from Firebase
    current_count = firebase.child('vote_count').get()

    # Increment the vote count
    new_count = current_count + 1 if current_count is not None else 1

    # Update the vote count in Firebase
    firebase.child('vote_count').set(new_count)

    # Return the updated vote count
    return new_count

# Route to receive scanned QR code data
@app.route('/scan', methods=['POST'])
def scan_qr_code():
    # Get the scanned data from the request
    data = request.json
    scanned_url = data.get('url')

    # Perform voting logic using the extracted URL
    vote_count = vote_increment()

    # Return a response with the updated vote count
    return jsonify({'message': 'Vote count incremented successfully', 'vote_count': vote_count})

if __name__ == '__main__':
    # Run the Flask app
    app.run(debug=True)
