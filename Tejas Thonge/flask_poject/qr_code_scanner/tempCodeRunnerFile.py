import requests

def send_scan_request(url):
    # URL of the Flask server
    server_url = 'http://127.0.0.1:5000/scan'

    # Data to be sent in the request
    data = {'url': url}

    try:
        # Send POST request to the server
        response = requests.post(server_url, json=data)

        # Print the response from the server
        print(response.json())

    except Exception as e:
        print("An error occurred:", e)

# Example usage: Send a POST request with the scanned URL
scanned_url = 'https://authdemo-b309c-default-rtdb.firebaseio.com/'
send_scan_request(scanned_url)
