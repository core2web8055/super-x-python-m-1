import tkinter as tk
from tkinter import messagebox, filedialog
import qrcode
import firebase_admin
from firebase_admin import credentials, firestore
from PIL import Image, ImageTk
# from pyzbar.pyzbar import decode

# Initialize Firebase with your configuration
firebase_config ={
  "apiKey" : "AIzaSyC4XcEc7hO_26sMaLIOuOIQWcalfihznKM",
  "authDomain": "qr-svm.firebaseapp.com",
  "databaseURL": "https://qr-svm-default-rtdb.firebaseio.com",
  "projectId": "qr-svm",
  "storageBucket": "qr-svm.appspot.com",
  "messagingSenderId": "1025192019715",
  "appId": "1:1025192019715:web:93a43ad015fe7fd81b4b36",
  "measurementId": "G-PQ2D47KQT5"
}

cred = credentials.Certificate("appBasic\serviceAccountKey.json")
firebase_admin.initialize_app(cred, firebase_config)
db = firestore.client()

class VoterApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Voter Software")
        
        # Initialize Firebase collection
        self.votes_collection = db.collection('votes')

        # Initialize UI elements
        self.qr_image_label = tk.Label(root)
        self.qr_image_label.pack(pady=10)
        self.scan_button = tk.Button(root, text="Scan QR Code", )
        self.scan_button.pack(pady=5)
        self.result_label = tk.Label(root, text="")
        self.result_label.pack(pady=10)

        # Generate QR codes for candidates
        self.generate_qr_codes()

    def generate_qr_codes(self):
        candidates = ["Candidate A", "Candidate B", "Candidate C"]
        for candidate in candidates:
            qr = qrcode.QRCode(version=1, box_size=10, border=5)
            qr.add_data(candidate)
            qr.make(fit=True)
            img = qr.make_image(fill='black', back_color='white')
            img.save(f"{candidate}.png")

#     def scan_qr(self):
#         # Open a file dialog to select the QR code image
#         file_path = filedialog.askopenfilename()
#         if file_path:
#             scanned_candidate = self.decode_qr(file_path)
#             if scanned_candidate:
#                 self.vote(scanned_candidate)
# 
#     def decode_qr(self, file_path):
#         # Decode the QR code image and extract candidate information
#         try:
#             qr_code = decode(Image.open(file_path))
#             if qr_code:
#                 candidate = qr_code[0].data.decode('utf-8')
#                 return candidate
#             else:
#                 messagebox.showerror("Error", "Failed to decode QR code.")
#                 return None
#         except Exception as e:
#             messagebox.showerror("Error", f"An error occurred: {str(e)}")
#             return None

    def vote(self, candidate):
        # Check if the voter has already voted (replace with actual authentication logic)
        voter_id = "12345"  # Example: Voter ID
        if self.check_voter_eligibility(voter_id):
            # Save the vote to Firebase
            self.save_vote(voter_id, candidate)
            messagebox.showinfo("Success", f"You've voted for {candidate}")
            self.result_label.config(text=f"Your vote has been recorded for {candidate}")
            
            # Increment the vote count for the candidate
            self.increment_vote_count(candidate)
        else:
            messagebox.showwarning("Warning", "You've already voted")

    def check_voter_eligibility(self, voter_id):
        # Check if the voter has already voted
        vote_doc = self.votes_collection.document(voter_id).get()
        return not vote_doc.exists

    def save_vote(self, voter_id, candidate):
        # Save the vote to Firebase
        self.votes_collection.document(voter_id).set({"candidate": candidate})

    def increment_vote_count(self, candidate):
        # Get the current vote count for the candidate from Firebase
        candidate_doc = db.collection('candidates').document(candidate).get()
        current_votes = candidate_doc.get('votes', 0)  # Default to 0 if no votes exist
        
        # Increment the vote count
        updated_votes = current_votes + 1
        
        # Update the vote count for the candidate in Firebase
        db.collection('candidates').document(candidate).update({'votes': updated_votes})


if __name__ == "__main__":
    root = tk.Tk()
    app = VoterApp(root)
    root.mainloop()
