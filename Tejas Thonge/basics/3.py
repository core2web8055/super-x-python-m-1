from tkinter import *
from tkinter import messagebox, simpledialog
import pyqrcode

class VotingSystem:
    def __init__(self):
      
        self.root = Tk()
        self.root.geometry("400x800")
        self.root.title("Voting System")

        self.matadar_sangh_label = Label(self.root, text="Enter Matadar Sangh:", font='time 15 bold')
        self.matadar_sangh_label.place(x=30, y=30)

        self.matadar_sangh_entry = Entry(self.root, width=27, bd=2, font="time 13 bold")
        self.matadar_sangh_entry.place(x=30, y=70)

        self.candidates = []
        self.candidates_label = Label(self.root, text="Enter Number of Candidates:", font='time 15 bold')
        self.candidates_label.place(x=30, y=110)

        self.candidates_entry = Entry(self.root, width=27, bd=2, font="time 13 bold")
        self.candidates_entry.place(x=30, y=150)

        self.candidates_button = Button(self.root, text="Submit", fg="white", bg="blue",
                                        font="time 15 bold", width=20, command=self.enter_candidates)
        self.candidates_button.place(x=30, y=190)

        self.matadar_sangh = ""
        self.no_of_candidates = 0
        self.voter_ids = set()

        self.root.mainloop()

    def enter_candidates(self):
        self.matadar_sangh = self.matadar_sangh_entry.get()
        try:
            self.no_of_candidates = int(self.candidates_entry.get())
            if self.no_of_candidates <= 0:
                messagebox.showerror("Error", "Number of candidates should be greater than 0.")
            else:
                self.candidates_label.config(text="Enter Candidate Names:")
                self.candidates_entry.destroy()
                self.candidates_button.config(command=self.generate_qr_codes)
                self.display_candidate_entry()

        except ValueError:
            messagebox.showerror("Error", "Please enter a valid number of candidates.")

    def display_candidate_entry(self):
        self.current_candidate = 0
        self.candidate_label = Label(self.root, text=f"Enter Candidate {self.current_candidate + 1} Name:", font='time 15 bold')
        self.candidate_label.place(x=30, y=230)

        self.candidate_entry = Entry(self.root, width=27, bd=2, font="time 13 bold")
        self.candidate_entry.place(x=30, y=270)

        self.next_button = Button(self.root, text="Next Candidate", fg="white", bg="blue",
                                  font="time 15 bold", width=20, command=self.enter_candidate)
        self.next_button.place(x=30, y=310)

    def enter_candidate(self):
        candidate_name = self.candidate_entry.get()
        if candidate_name:
            self.candidates.append(candidate_name)
            self.candidate_entry.delete(0, END)

            self.current_candidate += 1
            if self.current_candidate < self.no_of_candidates:
                self.candidate_label.config(text=f"Enter Candidate {self.current_candidate + 1} Name:")
            else:
                self.candidate_label.destroy()
                self.candidate_entry.destroy()
                self.next_button.destroy()

                self.candidates_button.config(text="Set Election", command=self.show_qr_codes)
                self.candidates_button.place(x=30, y=270)
        else:
            messagebox.showwarning("Incomplete Information", "Please enter the candidate name.")

    def generate_qr_codes(self):
        self.candidates = [entry.get() for entry in self.candidates_entries]

        for candidate in self.candidates:
            candidate_qr = pyqrcode.create(candidate)
            candidate_qr.png(f"{candidate}.png", scale=8)
            # candidate_qr.save(f"{candidate}.png")

    def show_qr_codes(self):
        for candidate in self.candidates:
            qr_image = PhotoImage(file=f"{candidate}.png")
            qr_label = Label(self.root, image=qr_image)
            qr_label.photo = qr_image
            qr_label.place(x=30, y=310 + (self.candidates.index(candidate) * 200))

if __name__ == "__main__":
    VotingSystem()
 