from tkinter import *
from tkinter import messagebox, simpledialog
import tkinter as tk
import pyqrcode
from random import randint

class VotingSystem:
    def __init__(self):
        self.root = Tk()
        self.root.geometry("400x800")
        self.root.title("Voting System")

        # Initializations for voting process

        self.matadar_sangh = ""
        self.candidate_names = []
        self.qr_codes = {}  # Dictionary to store candidate QR codes
        self.votes = {}  # Use a dictionary to store vote counts for each candidate
        self.voted_users = set()  # Track users who have already voted

        # Entry fields and labels for Matadar Sangh and number of candidates

        self.matadar_sangh_label = Label(self.root, text="Enter Matadar Sangh:", font='time 15 bold')
        self.matadar_sangh_label.place(x=30, y=30)

        self.matadar_sangh_entry = Entry(self.root, width=27, bd=2, font="time 13 bold")
        self.matadar_sangh_entry.place(x=30, y=70)

        self.candidates_label = Label(self.root, text="Enter Candidate Names (separated by commas):", font='time 15 bold')
        self.candidates_label.place(x=30, y=110)

        self.candidates_entry = Entry(self.root, width=40, bd=2, font="time 13 bold")
        self.candidates_entry.place(x=30, y=150)

        # Button to set up the election (generate QR codes, etc.)

        self.set_election_button = Button(self.root, text="Set Election", fg="white", bg="blue",
                                          font="time 15 bold", width=20, command=self.set_election)
        self.set_election_button.place(x=30, y=190)

        self.vote_labels = []  # List to store vote count labels for each candidate

        # Main event loop
        self.root.mainloop()

    def set_election(self):
        self.matadar_sangh = self.matadar_sangh_entry.get()
        self.candidate_names = [name.strip() for name in self.candidates_entry.get().split(',')]

        if not self.matadar_sangh or not self.candidate_names:
            messagebox.showerror("Error", "Please enter all required information.")
            return

        # Initialize votes dictionary with 0 votes for each candidate
        self.votes = {candidate: 0 for candidate in self.candidate_names}

        # Generate QR codes and store them in the dictionary

        for i, candidate in enumerate(self.candidate_names):
            unique_id = randint(10000, 99999)  # Ensure unique voter ID
            self.voted_users.add(unique_id)  # Add to voted_users set for tracking
            qr_data = f"{self.matadar_sangh}:{candidate}:{unique_id}"
            qr_code = pyqrcode.create(qr_data)
            qr_code.png(f"qr_code_{i}.png", scale=8)
            self.qr_codes[candidate] = f"qr_code_{i}.png"

        # Remove set_election button and create buttons for scanning QR codes

        self.set_election_button.destroy()
        for i, candidate in enumerate(self.candidate_names):
            # Display the initial vote count as 0 for each candidate
            label = Label(self.root, text=f"{candidate}: 0", font='time 15 bold')
            label.place(x=30, y=230 + (i * 50))
            self.vote_labels.append(label)

            scan_button = Button(self.root, text=f"Scan for {candidate}", fg="white", bg="blue",
                                 font="time 15 bold", width=20, command=lambda c=candidate: self.scan_qr_code(c))
            scan_button.place(x=30, y=260 + (i * 50))

    def scan_qr_code(self, candidate):
        try:
            user_id = simpledialog.askstring("Voter ID", "Enter your Voter ID:")
            if user_id not in self.voted_users:
                messagebox.showerror("Error", "Invalid Voter ID. Please try again.")
            else:
                self.votes[candidate] += 1
                self.voted_users.remove(user_id)
                messagebox.showinfo("Success", f"You have successfully voted for {candidate}.")

                # Update the display to reflect the new vote count
                self.update_display()
        except Exception as e:
            print(e)

    def update_display(self):
        # Update the text of each label to show the current vote count
        for i, candidate in enumerate(self.candidate_names):
            self.vote_labels[i].config(text=f"{candidate}: {self.votes[candidate]}")

if __name__ == "__main__":
    VotingSystem()
