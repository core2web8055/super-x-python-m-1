from flask import Flask, request, jsonify
import pyrebase
import uuid

app = Flask(__name__)

# Firebase configuration
firebase_config = {
    "apiKey": "AIzaSyCCtT2AIwDywDG4b6-WUSftVk-_G9yt2K0",
    "authDomain": "authdemo-b309c.firebaseapp.com",
    "databaseURL": "https://authdemo-b309c-default-rtdb.firebaseio.com",
    "projectId": "authdemo-b309c",
    "storageBucket": "authdemo-b309c.appspot.com",
    "messagingSenderId": "666875513320",
    "appId": "1:666875513320:web:9b7a1525169bbb06eefc5d",
    "measurementId": "G-WZ4BZL6HP5"
}

firebase = pyrebase.initialize_app(firebase_config)
db = firebase.database()

@app.route('/scan_qr', methods=['POST'])
def scan_qr():
    data = request.json
    qr_code_id = data.get('qr_code_id')

    if qr_code_id:
        # Update counter value in Firebase Realtime Database
        counter = db.child("qr_codes").child(qr_code_id).child("counter").get().val()
        if counter is not None:
            counter += 1
            db.child("qr_codes").child(qr_code_id).update({"counter": counter})
            return jsonify({'counter': counter})
        else:
            return jsonify({'error': 'QR code not found'}), 404
    else:
        return jsonify({'error': 'Missing QR code ID'}), 400

if __name__ == '__main__':
    app.run(debug=True)
