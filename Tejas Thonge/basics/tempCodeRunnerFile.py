import tkinter as tk
from PIL import Image, ImageTk
import requests
from io import BytesIO

def get_desktop_resolution(root):
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    return screen_width, screen_height

def load_image_from_url(url, width, height):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    img = img.resize((width, height), resample=Image.ANTIALIAS)
    return ImageTk.PhotoImage(img)

root = tk.Tk()

# Set title
root.title("Tkinter Window with Background Image from URL")

# Get desktop resolution
desktop_width, desktop_height = get_desktop_resolution(root)

# Set window size to match desktop size
root.geometry(f"{desktop_width}x{desktop_height}")

# URL of the image
image_url = "https://t4.ftcdn.net/jpg/03/01/88/91/360_F_301889163_tHkgenQP5srhNYmMUnOBTCVURCzEaTkK.jpg"  # Replace with your image URL

# Load and set background image from URL
bg_image = load_image_from_url(image_url, desktop_width, desktop_height)
bg_label = tk.Label(root, image=bg_image)
bg_label.place(relwidth=1, relheight=1)

# Add other widgets or elements as needed

root.mainloop()
