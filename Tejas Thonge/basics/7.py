import tkinter as tk
import requests
import qrcode
import io
from PIL import ImageTk, Image
from pyzbar.pyzbar import decode

import tkinter as tk
import requests
import qrcode
import io
from PIL import ImageTk, Image
from pyzbar.pyzbar import decode

# Firebase Realtime Database URL
firebase_database_url = "https://authdemo-b309c-default-rtdb.firebaseio.com/"

def generate_qr_and_store_in_firebase():
    # Generate unique identifier (UUID)
    qr_code_id = "your_generated_uuid_here"

    # Generate QR code for the UUID
    qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=10, border=4)
    qr.add_data(qr_code_id)
    qr.make(fit=True)
    qr_img = qr.make_image(fill_color="black", back_color="white")

    # Store QR code ID and initial vote count in Firebase Realtime Database
    firebase_data = {"counter": 0}
    response = requests.put(firebase_database_url + f"qr_codes/{qr_code_id}.json", json=firebase_data)

    # Convert QR code image to bytes
    qr_bytes = io.BytesIO()
    qr_img.save(qr_bytes, format='PNG')
    qr_bytes = qr_bytes.getvalue()

    return qr_bytes, qr_code_id

def scan_qr(event):
    # Get QR code data from the event
    qr_image = event.widget.qr_image
    decoded_data = decode(qr_image)[0].data.decode("utf-8")
    qr_code_id = decoded_data  # Decode QR code data

    # Retrieve data from Firebase
    response = requests.get(firebase_database_url + f"qr_codes/{qr_code_id}.json")
    qr_data = response.json()

    if qr_data is not None:
        counter = qr_data.get("counter", 0)  # Get current vote count
        counter += 1  # Increase vote count
        firebase_data = {"counter": counter}
        response = requests.patch(firebase_database_url + f"qr_codes/{qr_code_id}.json", json=firebase_data)  # Update vote count in Firebase
        print(f"Vote count increased for QR code {qr_code_id}. New count: {counter}")
    else:
        print(f"QR code {qr_code_id} not found in the database")

def show_qr_window(qr_image):
    root = tk.Tk()
    root.title("QR Code Scanner")

    img = Image.open(io.BytesIO(qr_image))
    img = img.resize((300, 300), Image.ANTIALIAS)
    qr_img = ImageTk.PhotoImage(img)

    label = tk.Label(root, image=qr_img)
    label.qr_image = qr_image  # Attach the QR image data to the label
    label.pack()

    # Bind a callback function to the left mouse click event
    label.bind('<Button-1>', scan_qr)

    root.mainloop()

if __name__ == "__main__":
    qr_image, qr_code_id = generate_qr_and_store_in_firebase()
    show_qr_window(qr_image)
from PIL import Image
import zbarlight

# Load image
with open('path_to_image.png', 'rb') as image_file:
    image = Image.open(image_file)
    image.load()

# Decode QR code
qr_code = zbarlight.scan_codes('qrcode', image)
if qr_code:
    print('QR code data:', qr_code[0].decode('utf-8'))
else:
    print('No QR code found.')


# Firebase Realtime Database URL
firebase_database_url = "https://authdemo-b309c-default-rtdb.firebaseio.com/"

def generate_qr_and_store_in_firebase():
    # Generate unique identifier (UUID)
    qr_code_id = "your_generated_uuid_here"

    # Generate QR code for the UUID
    qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=10, border=4)
    qr.add_data(qr_code_id)
    qr.make(fit=True)
    qr_img = qr.make_image(fill_color="black", back_color="white")

    # Store QR code ID and initial vote count in Firebase Realtime Database
    firebase_data = {"counter": 0}
    response = requests.put(firebase_database_url + f"qr_codes/{qr_code_id}.json", json=firebase_data)

    # Convert QR code image to bytes
    qr_bytes = io.BytesIO()
    qr_img.save(qr_bytes, format='PNG')
    qr_bytes = qr_bytes.getvalue()

    return qr_bytes, qr_code_id

def scan_qr(event):
    # Get QR code data from the event
    qr_image = event.widget.qr_image
    decoded_data = decode(qr_image)[0].data.decode("utf-8")
    qr_code_id = decoded_data  # Decode QR code data

    # Retrieve data from Firebase
    response = requests.get(firebase_database_url + f"qr_codes/{qr_code_id}.json")
    qr_data = response.json()

    if qr_data is not None:
        counter = qr_data.get("counter", 0)  # Get current vote count
        counter += 1  # Increase vote count
        firebase_data = {"counter": counter}
        response = requests.patch(firebase_database_url + f"qr_codes/{qr_code_id}.json", json=firebase_data)  # Update vote count in Firebase
        print(f"Vote count increased for QR code {qr_code_id}. New count: {counter}")
    else:
        print(f"QR code {qr_code_id} not found in the database")

def show_qr_window(qr_image):
    root = tk.Tk()
    root.title("QR Code Scanner")

    img = Image.open(io.BytesIO(qr_image))
    img = img.resize((300, 300), Image.ANTIALIAS)
    qr_img = ImageTk.PhotoImage(img)

    label = tk.Label(root, image=qr_img)
    label.qr_image = qr_image  # Attach the QR image data to the label
    label.pack()

    # Bind a callback function to the left mouse click event
    label.bind('<Button-1>', scan_qr)

    root.mainloop()

if __name__ == "__main__":
    qr_image, qr_code_id = generate_qr_and_store_in_firebase()
    show_qr_window(qr_image)
