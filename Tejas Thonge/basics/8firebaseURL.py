import tkinter as tk
from firebase import firebase
# Initialize Firebase with your database URL
firebase = firebase.FirebaseApplication('https://authdemo-b309c-default-rtdb.firebaseio.com/', None)

def vote():
    # Retrieve the current vote count from Firebase
    current_count = firebase.get('/vote_count', None)

    # Increment the vote count
    new_count = current_count + 1 if current_count is not None else 1

    # Update the vote count in Firebase
    firebase.put('/', 'vote_count', new_count)

    # Update the label text to display the new count
    vote_label.config(text=f"Total Votes: {new_count}")

# Create the main Tkinter window
root = tk.Tk()
root.title("Vote App")

# Create a label to display the vote count
vote_label = tk.Label(root, text="Total Votes: 0")
vote_label.pack()

# Create a button to cast a vote
vote_button = tk.Button(root, text="Vote", command=vote)
vote_button.pack()

root.mainloop()
