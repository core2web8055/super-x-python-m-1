from tkinter import  *
from tkinter import messagebox
import pyrebase

root = Tk()
root.geometry("400x800")
root.title("FireBase App")

Config = {
  "apiKey": "AIzaSyCCtT2AIwDywDG4b6-WUSftVk-_G9yt2K0",
  "authDomain": "authdemo-b309c.firebaseapp.com",
  "databaseURL": "https://authdemo-b309c-default-rtdb.firebaseio.com",
  "projectId": "authdemo-b309c",
  "storageBucket": "authdemo-b309c.appspot.com",
  "messagingSenderId": "666875513320",
  "appId": "1:666875513320:web:9b7a1525169bbb06eefc5d",
  "measurementId": "G-WZ4BZL6HP5"
}

firebase = pyrebase.initialize_app(Config)
db = firebase.database()

def dataConnection():
    name = e1.get()
    city = e2.get()
    voterid = e3.get()
    data = {
        "username": name,
        "city": city,
        "voterid": voterid
    }
    db.child("user").push(data)
    messagebox.showinfo("Information", "Data inserted")

l1 = Label(root, text="FireBase App", font="time 17 bold", foreground="yellow")
l1.place(x=30, y=30)

l2 = Label(root, text="Enter the username", font='time 15 bold')
l2.place(x=30, y=90)

e1 = Entry(root, width=27, bd=2, font="time 13 bold")
e1.place(x=30, y=130)

l3 = Label(root, text="Enter the city", font="time 17 bold")
l3.place(x=30, y=170)

e2 = Entry(root, width=27, bd=2, font="time 13 bold")
e2.place(x=30, y=200)

l4 = Label(root, text="Enter the Voter ID", font="time 17 bold")
l4.place(x=30, y=230)

e3 = Entry(root, width=27, bd=2, font="time 13 bold")
e3.place(x=30, y=260)

b1 = Button(root, text="Submit", fg="white", bg="blue", font="time 15 bold", width=20, command=dataConnection)
b1.place(x=30, y=300)

root.mainloop()
