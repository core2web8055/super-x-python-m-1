from flask import Flask, request, jsonify
from firebase import firebase

app = Flask(__name__)

# Initialize Firebase with your database URL
firebase = firebase.FirebaseApplication('https://authdemo-b309c-default-rtdb.firebaseio.com/', None)

# Function to increment the vote count in Firebase
def vote_increment():
    # Retrieve the current vote count from Firebase
    current_count = firebase.get('/vote_count', None)

    # Increment the vote count
    new_count = current_count + 1 if current_count is not None else 1

    # Update the vote count in Firebase
    firebase.put('/', 'vote_count', new_count)

    # Return the updated vote count
    return new_count

# Route to receive scanned QR code data
@app.route('/scan', methods=['POST'])
def scan_qr_code():
    # Get the scanned data from the request
    data = request.json
    scanned_url = data.get('url')

    # Perform voting logic using the extracted URL
    vote_count = vote_increment()

    # Return a response with the updated vote count
    return jsonify({'message': 'Vote count incremented successfully', 'vote_count': vote_count})

if __name__ == '__main__':
    # Run the Flask app
    app.run(debug=True)
