import tkinter as tk
from PIL import Image, ImageTk
from io import BytesIO
from flask_poject.flask_server.qr_code_generator import generate_qr_code

class QRCodeApp:
    def __init__(self, root):
        self.root = root
        self.root.title("QR Code Generator")

        # URL to encode in the QR code
        self.qr_data = "http://192.168.167.505500/scan" # Replace with your desired URL

        # Generate the QR code image
        self.qr_image_data = generate_qr_code(self.qr_data)
        self.qr_image = ImageTk.PhotoImage(Image.open(BytesIO(self.qr_image_data)))

        # Create and pack the QR code label
        self.qr_label = tk.Label(root, image=self.qr_image)
        self.qr_label.pack()

if __name__ == "__main__":
    root = tk.Tk()
    app = QRCodeApp(root)
    root.mainloop()
