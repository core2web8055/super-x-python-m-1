import qrcode
from io import BytesIO
from flask import send_file

def generate_qr_code(data):
    # Create a QR code instance
    qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_L, box_size=10, border=4)

    # Add data to the QR code
    qr.add_data(data)
    qr.make(fit=True)

    # Create an image from the QR code
    img = qr.make_image(fill_color="black", back_color="white")

    # Save the image to a BytesIO buffer
    img_buffer = BytesIO()
    img.save(img_buffer, format="PNG")

    # Return the image buffer
    return img_buffer.getvalue()
