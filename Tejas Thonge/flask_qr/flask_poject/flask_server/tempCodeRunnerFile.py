from flask import Flask, request, jsonify
from firebase_integration import firebase
from qr_code_generator import generate_qr_code
from firebase_integration import initialize_app, database

app = Flask(__name__)

# Initialize Firebase with the credentials file and database URL
firebase.initialize_app()

# Reference to your Firebase database
db = firebase.database()

# Function to increment the vote count in Firebase
def vote_increment():
    # Retrieve the current vote count from Firebase
    current_count = db.child('vote_count').get()

    # Increment the vote count
    new_count = current_count + 1 if current_count is not None else 1

    # Update the vote count in Firebase
    db.child('vote_count').set(new_count)

    # Return the updated vote count
    return new_count

# Route to receive scanned QR code data
@app.route('/scan', methods=['POST'])
def scan_qr_code():
    # Get the scanned data from the request
    data = request.json
    scanned_url = data.get('url')

    # Perform voting logic using the extracted URL
    vote_count = vote_increment()

    # Return a response with the updated vote count
    return jsonify({'message': 'Vote count incremented successfully', 'vote_count': vote_count})

# Route to generate and return QR code image
@app.route('/generate_qr_code')
def generate_qr():
    qr_data = "ttp://127.0.0.1:5000/scan"  # Example URL to encode in QR code
    qr_image_data = generate_qr_code(qr_data)
    return qr_image_data, 200, {'Content-Type': 'image/png'}

if __name__ == '__main__':
    # Run the Flask app
    app.run(debug=True)
