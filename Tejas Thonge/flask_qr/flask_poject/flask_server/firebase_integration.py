from firebase_admin import credentials, initialize_app, db

def initialize_firebase_app():
    cred = credentials.Certificate("credentials.json")
    initialize_app(cred, {
        'databaseURL': 'https://authdemo-b309c-default-rtdb.firebaseio.com/'
    })

def database():
    return db.reference()
