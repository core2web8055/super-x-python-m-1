# src/login.py
import tkinter as tk
from PIL import Image, ImageTk
import requests
from io import BytesIO






class SplashScreen:
    def __init__(self, root, duration=3):
        self.root = root
        self.root.title("Splash Screen")

        # Get desktop resolution
        self.desktop_width, self.desktop_height = self.get_desktop_resolution()

        # Set window size to match desktop size
        self.root.geometry(f"{self.desktop_width}x{self.desktop_height}")

        # URL of the splash image
        self.image_url = "https://img.etimg.com/thumb/width-1600,height-900,imgsize-338499,resizemode-75,msid-106052049/news/elections/lok-sabha/india/the-vote-worthy-how-are-political-parties-planning-to-connect-with-millions-of-first-time-voters-in-2024-elections.jpg" # Replace with your image URL

        # Load and set splash image from URL, resizing to fit the window
        self.splash_image = self.load_image_from_url(self.image_url, self.desktop_width, self.desktop_height)
        self.splash_label = tk.Label(root, image=self.splash_image)
        self.splash_label.place(relwidth=1, relheight=1)

        # Set the duration (in seconds) for the splash screen
        self.duration = duration

        # After the specified duration, destroy the splash screen and open the login screen
        self.root.after(int(self.duration * 1000), self.destroy_splash)

    def get_desktop_resolution(self):
        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        return screen_width, screen_height

    def load_image_from_url(self, url, target_width, target_height):
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))
        img = img.resize((target_width, target_height), Image.ANTIALIAS)
        return ImageTk.PhotoImage(img)

    def destroy_splash(self):
        # Destroy the splash screen and open the login screen
        self.root.destroy()
        root = tk.Tk()
        login_screen = LoginScreen(root)
        root.mainloop()


class LoginScreen:
    def __init__(self, root):
        self.root = root
        self.root.title("Login Screen")

        # Get desktop resolution
        self.desktop_width, self.desktop_height = self.get_desktop_resolution()

        # Set window size to match desktop size
        self.root.geometry(f"{self.desktop_width}x{self.desktop_height}")

        # URL of the background image
        self.image_url = "https://t4.ftcdn.net/jpg/03/01/88/91/360_F_301889163_tHkgenQP5srhNYmMUnOBTCVURCzEaTkK.jpg"  # Replace with your image URL

        # Load and set background image from URL, resizing to fit the window
        self.bg_image = self.load_image_from_url(self.image_url, self.desktop_width, self.desktop_height)
        self.bg_label = tk.Label(root, image=self.bg_image)
        self.bg_label.place(relwidth=1, relheight=1)

        # Add login widgets
        self.margin = 20  # Adjust the margin as needed

        # Customize font for larger, bold text
        custom_font = ("Helvetica", 16, "bold")

        # Frame to hold the login widgets
        login_frame = tk.Frame(root, highlightthickness=0)  # Set highlightthickness to 0 to remove the background color
        login_frame.place(x=1200, y=400, anchor="s")  # Center the frame

        self.id_type_label = tk.Label(login_frame, text="Select ID Type:", font=custom_font)
        self.id_type_var = tk.StringVar()
        self.id_type_var.set("Aadhar")  # Default selection
        self.id_type_dropdown = tk.OptionMenu(login_frame, self.id_type_var, "Aadhar", "Voter ID")

        self.id_number_label = tk.Label(login_frame, text="Number:", font=custom_font)
        self.id_number_entry_aadhar = tk.Entry(login_frame, font=custom_font)
        self.id_number_entry_voter = tk.Entry(login_frame, font=custom_font)

        self.login_button = tk.Button(login_frame, text="vote for", command=self.login, font=custom_font
                                      
                                      )

        # Place login widgets using the grid layout
        self.id_type_label.grid(row=0, column=0, sticky="w", pady=self.margin)
        self.id_type_dropdown.grid(row=0, column=1, sticky="w", pady=self.margin)

        self.id_number_label.grid(row=1, column=0, sticky="w", pady=self.margin)
        self.id_number_entry_aadhar.grid(row=1, column=1, sticky="w", pady=self.margin)
        self.id_number_entry_voter.grid(row=2, column=1, sticky="w", pady=self.margin)
        self.id_number_entry_voter.grid_remove()  # Hide the Voter ID entry initially

        self.login_button.grid(row=3, column=1, sticky="w", pady=self.margin)

        # Event to toggle visibility of ID entry based on ID type selection
        self.id_type_var.trace_add("write", self.toggle_id_entry)

    def get_desktop_resolution(self):
        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        return screen_width, screen_height

    def load_image_from_url(self, url, target_width, target_height):
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))
        img = img.resize((target_width, target_height), Image.ANTIALIAS)
        return ImageTk.PhotoImage(img)

    def toggle_id_entry(self, *args):
        # Toggle visibility of ID entry based on ID type selection
        if self.id_type_var.get() == "Aadhar":
            self.id_number_entry_aadhar.grid()
            self.id_number_entry_voter.grid_remove()
        elif self.id_type_var.get() == "Voter ID":
            self.id_number_entry_voter.grid()
            self.id_number_entry_aadhar.grid_remove()

    def login(self):
        # Add your login logic here
        id_type = self.id_type_var.get()
        id_number = ""
        if id_type == "Aadhar":
            id_number = self.id_number_entry_aadhar.get()
        elif id_type == "Voter ID":
            id_number = self.id_number_entry_voter.get()

        print(f"{id_type}: {id_number}")

if __name__ == "__main__":
    root = tk.Tk()
    splash_screen = SplashScreen(root)
    root.mainloop()
