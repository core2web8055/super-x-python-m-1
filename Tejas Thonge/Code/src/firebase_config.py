import firebase_admin
from firebase_admin import credentials, firestore

# Path to your Firebase Admin SDK JSON file
cred_path = "src\credentials.json"

# Initialize Firebase Admin SDK
cred = credentials.Certificate(cred_path)
firebase_admin.initialize_app(cred)

# Initialize Firestore
db = firestore.client()
