# main.py
import tkinter as tk
from src.login import LoginScreen

if __name__ == "__main__":
    root = tk.Tk()
    login_screen = LoginScreen(root)
    root.mainloop()
